from rest_framework import permissions, renderers, viewsets
from rest_framework.decorators import action

from chemical.models import Molecule, Commodity, Chemical
from chemical.permissions import IsOwnerOrReadOnly
from chemical.serializers import MoleculeSerializer, CommoditySerializer, ChemicalSerializer, ChemicalModelSerializer, \
    CommodityModelSerializer

from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class MoleculeViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Molecule.objects.all()
    serializer_class = MoleculeSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,)

    @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        snippet = self.get_object()
        return Response(snippet.highlighted)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class CommodityViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Commodity.objects.all()
    serializer_class = CommodityModelSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,)

    @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        snippet = self.get_object()
        return Response(snippet.highlighted)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class ChemicalModelViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Chemical.objects.all()
    serializer_class = ChemicalModelSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,)

    @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        snippet = self.get_object()
        return Response(snippet.highlighted)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_object(self, pk):
        try:
            return Chemical.objects.get(pk=pk)
        except Chemical.DoesNotExist:
            raise Http404

    def destroy(self, request, pk, format=None):
        object = self.get_object(pk)
        commodity = object.commodity
        owner = object.owner
        object.delete()

        if not Molecule.objects.filter(name='Unknown', owner=owner).exists():
            molecule_instance = Molecule.objects.create(name='Unknown', owner=owner)
        else:
            molecule_instance = Molecule.objects.get(name='Unknown', owner=owner)

        Chemical_data = Chemical.objects.filter(commodity=commodity, owner=owner).exclude(molecule=molecule_instance)
        percentage_value = 0
        for chemical in Chemical_data:
            percentage_value += chemical.percentage

        # print(percentage_value)
        total = 100

        unknow_per = total - percentage_value
        # print(unknow_per)

        if Chemical.objects.filter(molecule=molecule_instance, commodity=commodity,
                                   owner=owner).exists():
            chemical_instance = Chemical.objects.get(molecule=molecule_instance, owner=owner,
                                                     commodity=commodity)
        else:
            chemical_instance = Chemical()
            chemical_instance.commodity = commodity
            chemical_instance.molecule = molecule_instance
        chemical_instance.percentage = unknow_per
        chemical_instance.owner = owner
        chemical_instance.save()
        print('delete')
        return Response(status=status.HTTP_204_NO_CONTENT)
