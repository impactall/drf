from django.db import models


class Molecule(models.Model):
    name = models.CharField(max_length=100, default='', null=None)
    owner = models.ForeignKey(
        'auth.User', related_name='molecule', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Commodity(models.Model):
    name = models.CharField(max_length=100, null=None, default='')
    inventory = models.CharField(max_length=100, null=None, default='')
    price = models.CharField(max_length=100, null=None, default='')
    owner = models.ForeignKey(
        'auth.User', related_name='commodity', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Commodity"
        verbose_name_plural = "Commodity"

    # def clean(self):
    #     # Sum of components must be 100
    #     total_sum = sum(comp.percentage for comp in self.chemical.object.all())
    #     if total_sum > 100:
    #         raise ValidationError('Sum of components must be 100%')
    #     elif total_sum < 100:
    #         # self.chemical_set.add()
    #         raise ValidationError('Sum of components less be 100%')

    def save(self, *args, **kwargs):
        super(Commodity, self).save(*args, **kwargs)


class Chemical(models.Model):
    commodity = models.ForeignKey(Commodity, related_name='chemical_composition', on_delete=models.CASCADE,
                                  null=None)
    molecule = models.ForeignKey(Molecule, related_name='chemical_molecule_source', on_delete=models.CASCADE, null=None)
    percentage = models.FloatField(max_length=100, null=None, default='')
    owner = models.ForeignKey('auth.User', related_name='chemical', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        super(Chemical, self).save(*args, **kwargs)
