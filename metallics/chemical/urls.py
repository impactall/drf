from rest_framework.routers import DefaultRouter
from chemical import views
from django.urls import include, path

router = DefaultRouter()
router.register(r'Chemical Elements', views.MoleculeViewSet)
router.register(r'Chemical Concentration', views.ChemicalModelViewSet)
router.register(r'Commodity', views.CommodityViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
