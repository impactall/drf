from rest_framework import serializers
from chemical.models import Molecule, Commodity, Chemical
from django.core.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework import status


class MoleculeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Molecule
        fields = ('id', 'name')


class ChemicalSerializer(serializers.HyperlinkedModelSerializer):
    def to_representation(self, obj):
        data = super(ChemicalSerializer, self).to_representation(obj)
        element = {}
        element['id'] = obj.molecule.id
        element['name'] = obj.molecule.name
        data["element"] = element
        return data

    class Meta:
        model = Chemical
        fields = ['percentage']


class CommoditySerializer(serializers.HyperlinkedModelSerializer):
    chemical_composition = ChemicalSerializer(many=True)

    class Meta:
        model = Commodity
        fields = ('id', 'name', 'price', 'inventory', 'chemical_composition')


class ChemicalModelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Chemical
        fields = ('commodity', 'molecule', 'percentage')

    def create(self, validated_data):
        if not Molecule.objects.filter(name='Unknown', owner=validated_data.get('owner')).exists():
            molecule_instance = Molecule.objects.create(name='Unknown', owner=validated_data.get('owner'))
        else:
            molecule_instance = Molecule.objects.get(name='Unknown', owner=validated_data.get('owner'))

        Chemical_data = Chemical.objects.filter(commodity=validated_data.get('commodity'),
                                                owner=validated_data.get('owner')).exclude(molecule=molecule_instance)
        percentage_value = 0
        for chemical in Chemical_data:
            percentage_value += chemical.percentage

        total = 100
        total_get = percentage_value + validated_data.get('percentage')
        unknow_per = total - total_get
        if total_get > 100:
            raise serializers.ValidationError("Percentage is not greater than 100.")

        if Chemical.objects.filter(molecule=molecule_instance, commodity=validated_data.get('commodity'),
                                   owner=validated_data.get('owner')).exists():
            chemical_instance = Chemical.objects.get(molecule=molecule_instance, owner=validated_data.get('owner'),
                                                     commodity=validated_data.get('commodity'))
        else:
            chemical_instance = Chemical()
            chemical_instance.commodity = validated_data.get('commodity')
            chemical_instance.molecule = molecule_instance
        chemical_instance.percentage = unknow_per
        chemical_instance.owner = validated_data.get('owner')
        chemical_instance.save()

        instance = Chemical.objects.create(**validated_data)
        return instance

    def update(self, instance, validated_data):
        if not Molecule.objects.filter(name='Unknown', owner=instance.owner).exists():
            molecule_instance = Molecule.objects.create(name='Unknown', owner=instance.owner)
        else:
            molecule_instance = Molecule.objects.get(name='Unknown', owner=instance.owner)

        instance.commodity = validated_data.get('commodity')
        instance.molecule = validated_data.get('molecule')
        instance.percentage = validated_data.get('percentage')
        instance.owner = instance.owner
        instance.save()

        Chemical_data = Chemical.objects.filter(commodity=validated_data.get('commodity'),
                                                owner=instance.owner).exclude(molecule=molecule_instance)
        percentage_value = 0
        for chemical in Chemical_data:
            percentage_value += chemical.percentage

        total = 100
        unknow_per = total - percentage_value

        if percentage_value > 100:
            raise serializers.ValidationError("Percentage is not greater than 100.")

        if Chemical.objects.filter(molecule=molecule_instance, commodity=validated_data.get('commodity'),
                                   owner=instance.owner).exists():
            chemical_instance = Chemical.objects.get(molecule=molecule_instance, owner=instance.owner,
                                                     commodity=validated_data.get('commodity'))
        else:
            chemical_instance = Chemical()
            chemical_instance.commodity = validated_data.get('commodity')
            chemical_instance.molecule = molecule_instance
        chemical_instance.percentage = unknow_per
        chemical_instance.owner = instance.owner
        chemical_instance.save()
        return instance


class CommodityModelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Commodity
        fields = ('id', 'name', 'price', 'inventory')
