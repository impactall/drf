from django.contrib import admin

from .models import Chemical, Commodity, Molecule


class ChoiceInline(admin.TabularInline):
    model = Chemical
    extra = 3

    # def save_model(self, request, obj, formset, change):
    #     # instances = formset.save(commit=False)
    #     # for instance in instances:
    #     #     print('hello')
    #     #     exit()
    #     #     # Do something with `instance`
    #     #     instance.save()
    #     print(obj.related_set.all())
    #     # print(formset)
    #     exit(0)
    #     formset.save_m2m()


class CommodityAdmin(admin.ModelAdmin):
    # fieldsets = [
    #     (None,               {'fields': ['question_text']}),
    #     ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    # ]
    inlines = [ChoiceInline]


admin.site.register(Molecule)
admin.site.register(Commodity, CommodityAdmin)
admin.site.register(Chemical)
