# REST framework tutorial

Source code for the [Django REST framework tutorial][tut].

[tut]: http://www.django-rest-framework.org/tutorial/1-serialization

This API is developed on django rest framework.

1) Need to install python3 and create virtual env 
2) Install required packages by command
 "pip install -r requirements.txt"
3) Run application by command
"python manage.py runserver"

4) open application on browser
http://localhost:8000/api/ 

Authentication credential
Username: admin
Password: admin@123