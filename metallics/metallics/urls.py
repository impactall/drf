from django.conf.urls import url
from django.urls import include, path

from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view

API_TITLE = 'Metallics API'
API_DESCRIPTION = 'A Web API for creating and viewing highlighted code ' + API_TITLE + '.'

schema_view = get_swagger_view(title=API_TITLE)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', schema_view),
    path('', include('chemical.urls')),
]
